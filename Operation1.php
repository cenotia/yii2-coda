<?php

namespace cenotia\components\coda;


class Operation1 extends Operation {

	public $amount;


	public $seq_extrait;
	public $credit;
	public $code;
	public $date_val;
	public $date_compta;
	public $communication;
	public $communication_struct;
	public $ref_bank;

	public function rules() {
		return \yii\helpers\ArrayHelper::merge(parent::rules(), 		
		[
			['amount','amount',32,15],
			['detail_num','number',6,4],
			['seq','number',2,4],
			['link','number',127,1],
			['ref_bank','number',10,21],
			['date_val','date',47,6],
			['date_compta','date',115,6],
			['seq_extrait','number',121,3],
			['suit_code','number',125,1],
			['credit','boolean',31,1,"0"],			
			['if'=>[0=>['boolean',61,1,"1"]], 'set' => ['communication','string',62,52]],
			['if'=>[1=>['boolean',61,1,"1"]], 'set' => ['communication_struct','record',62,52,Communication::className()]],
			['code','record',53,8,OperationCode::className()],
		]);
	}


}