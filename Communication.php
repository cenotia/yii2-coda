<?php

namespace cenotia\components\coda;


class Communication extends Record {

	public $type;
	public $communication;


	public function rules() {
		return [
			['type','number',0,3],
			['communication','string',3,49],
		];
	}


}