<?php

namespace cenotia\components\coda;


class API {

	protected $file;

	protected $coda;

	public function __construct($file) {
		$this->file = $file;
	}

	public function decode() {
		$handle = fopen($this->file, "r");
		$records = [];
		if ($handle) {
    		while (($line = fgets($handle)) !== false) {
        		$records[] = Record::getInstance($line);
    		}
    		fclose($handle);

    		$this->generate($records);
    		return $this->coda;
		} else {
    		throw new \Exception("file not found");
		} 
	}


	public function generate($records) {
		$lastSeq = -1;
		$lastRef= -1;
		$this->coda = new Coda();

		foreach ($records as $record) {
			if ($record instanceof Header) {
				$this->coda->header = $record;
			}
			else if ($record instanceof Footer) {
				$this->coda->footer = $record;
			}
			else if ($record instanceof StartBalance) {
				$this->coda->startBalance = $record;
			}
			else if ($record instanceof EndBalance) {
				$this->coda->endBalance = $record;
			}
			else if ($record instanceof Operation) {				

				if ($record instanceof Operation1) {
					if ($record->ref_bank != $ref_bank || $record->seq != $lastSeq) {
						$this->coda->operations[] = $record;
						$ref_bank = $record->ref_bank;
						$lastSeq = $record->seq;
					}
					else {
						$this->coda->operations[count($this->coda->operations) -1 ]->details[] = $record;	
					}

				}
				else {
					if ($record->seq != $lastSeq) {
						$this->coda->operations[] = $record;
						$lastSeq = $record->seq;
					}
					else {
						$this->coda->operations[count($this->coda->operations) -1 ]->details[] = $record;	
					}
				}


			}
 		}
	}

	public function getCoda() {
		return $this->coda;
	}
}