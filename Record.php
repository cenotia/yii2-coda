<?php

namespace cenotia\components\coda;


abstract class Record extends \yii\base\Object {

	protected $line;

	public function __construct($line) {
		$this->line = $line;
		$this->initFields();
	}

	public static function getInstance($line) {
		switch (substr($line, 0,1)) {
			case "0": return new Header($line);
			  		  break;
			case "1": return new StartBalance($line);
			  		  break;
			case "2": if (substr($line,1,1) == "1") return new Operation1($line);
					  if (substr($line,1,1) == "2") return new Operation2($line);
					  return new Operation3($line);
			  		  break;
			case "3": return new Information($line);
			  		  break;
			case "4": return new Message($line);
			  		  break;
			case "8": return new EndBalance($line);
			  		  break;
			case "9": return new Footer($line);
			  		  break;
			default: throw new \Exception("not implemented " . substr($line,1,1));
		}
	}


	public function rules () {
		return [];
	}

	public function initFields() {
		$rules = $this->rules();
		if (!$rules || count($rules) === 0) return;


		foreach ($rules as $rule) {
			if (array_key_exists('if', $rule)) {
				$ok = true;
				foreach ($rule['if'] as $key => $value) {
					$v = $this->getField($value);
					if ($key != $v) $ok = false;	
				}
				if ($ok) {
					$this->setField($rule['set']);
				}

			}
			else {
				
				$this->setField($rule);
			}
		}
	}

	protected function setField($rule) {
			$r = $rule;
			$field = array_shift($r);
			if ($field)
			$this->$field  = $this->getField($r);
	}


	protected function getField($rule) {


			
			switch ($rule[0]) {
				case 'string': return $this->getString($rule[1],$rule[2]);
								break;
				case 'date': return $this->getDate($rule[1],$rule[2]);
								break;
				case 'amount': return $this->getAmountR($rule[1],$rule[2]);
								break;
				case 'number': return $this->getNumber($rule[1],$rule[2]);
								break;
				case 'record': return $this->getRecord($rule[1],$rule[2],$rule[3]);
								break;
				case 'boolean': return $this->getBoolean($rule[1],$rule[2],$rule[3]);
								break;

			}

	}

	protected function getAmountR($start, $cnt) {
		$str = substr($this->line,$start,$cnt);
		return floatval($str) / 1000;
	}

	protected function getNumber($start, $cnt) {
		$str = substr($this->line,$start,$cnt);
		return intval($str) ;
	}

	protected function getDate($start, $cnt) {
		$str = substr($this->line,$start,$cnt);
		return \DateTime::createFromFormat("dmy",$str);
	}

	protected function getString($start, $cnt) {
		$str = substr($this->line,$start,$cnt);
		return $str;
	}

	protected function getBoolean($start, $cnt,$cmp) {
		$str = substr($this->line,$start,$cnt);
		return ($str === $cmp)?1:0;
	}

	protected function getRecord($start, $cnt,$className) {
		$str = substr($this->line,$start,$cnt);
		$record = new $className($str);
		$record->initfields();
		return $record;
	}

	public function getTextDescription() {
		return print_r($this,true);
	}
}