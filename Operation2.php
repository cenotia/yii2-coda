<?php

namespace cenotia\components\coda;


class Operation2 extends Operation {

	public $transaction_type;
	public $communication;

	public function rules() {
		return \yii\helpers\ArrayHelper::merge(parent::rules(), 		
		[
			['transaction_type','string',112,1],
			['communication','string',10,53]
		]);

	}


}