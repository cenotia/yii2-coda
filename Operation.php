<?php

namespace cenotia\components\coda;


class Operation extends Record {

	public $details;
	public $infos;
	public $seq;
	public $detail_num;
	public $suit_code;
	public $link;


	public function rules() {
		return [
			['detail_num','number',6,4],
			['seq','number',2,4],
			['link','number',127,1],
			['suit_code','number',125,1],
		];
	}


}