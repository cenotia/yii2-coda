<?php

namespace cenotia\components\coda;


class EndBalance extends Record {

	public $balance;
	public $date;


	public function rules() {
		return [
			['balance','amount',42,15],
			['date','date',57,6]
		];
	}



}