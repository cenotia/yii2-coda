<?php

namespace cenotia\components\coda;


class Operation3 extends Operation {

	public $name;
	public $iban;

	public function rules() {
		return \yii\helpers\ArrayHelper::merge(parent::rules(), [
			['name','string',47,40],
			['iban','string',10,16]
		]);
	}
	


}