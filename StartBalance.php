<?php

namespace cenotia\components\coda;


class StartBalance extends Record {

	public $balance;
	public $date;
	public $account_label;
	public $credit;
	public $titulaire;


	public function rules() {
		return [
			['balance','amount',43,15],
			['date','date',58,6],
			['credit','boolean',42,1,"0"],
			['account_label','string',90,35],
			['titulaire','string',64,26],
		];
	}



}