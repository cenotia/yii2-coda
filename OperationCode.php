<?php

namespace cenotia\components\coda;


class OperationCode extends Record {

	protected $type;
	public $family;
	protected $operation;
	protected $rubrique;


	public function rules() {
		return [
			['type','number',0,1],
			['family','number',1,2],
			['operation','number',3,2],
			['rubrique','number',5,3],			
		];
	}

	public function getFamilyName() {
		if (!$this->family) return "";
		if ($this->family==1) return \yii::t('app','Virement');
		if ($this->family==3) return \yii::t('app','Chèque');		
		if ($this->family==4) return \yii::t('app','Paiement par carte');
		if ($this->family==5) return \yii::t('app','Domiciliation');
		if ($this->family==13) return \yii::t('app','Crédit');
		return "";
	}
}