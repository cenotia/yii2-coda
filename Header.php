<?php

namespace cenotia\components\coda;


class Header extends Record {

	protected $creation_date;
	protected $bank_id_num;
	protected $duplicata;
	protected $to_name;
	protected $bic;
	protected $tit_identification_num;


	public function rules() {
		return [
			['creation_date','date',5,6],
			['bank_id_num','string',11,3],
			['duplicata','boolean',16,1,"D"],
			['to_name','string',34,26],
			['bic','string',60,11],
			['tit_identification_num','string',71,11],
		];
	}


}