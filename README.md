cenotia/yii2-coda
===================

[TOC]

Installation
-------------

### composer.json
```
 "require": {
    "cenotia/yii2-coda": "dev-master"
 },
 "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/cenotia/yii2-coda.git"
        }
  ]
```

In your project